#!/bin/bash
declare -a arr=( $( ps aux | grep [L]ockExpiredAutoSetupJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -eq 0 ]; then
   echo "LockExpiredAutoSetupJobDefinition :- Job Started -$(date)"
   java -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE -jar /home/ubuntu/scheduler/lenderprice-scheduler-client.jar com.cre8techlabs.scheduler.job.lockStatusPolicy.LockExpiredAutoSetupJobDefinition >>  /home/ubuntu/logs/LockExpiredAutoSetupJobDefinitionlogs/`date +\%Y\%m\%d`.log 2>&1
else
    echo "LockExpiredAutoSetupJobDefinition :- ${arr[@]} -Job already processing -$(date)"
fi