#!/bin/bash
declare -a arr=( $( ps aux | grep [R]ateAlertJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -eq 0 ]; then
   echo "RateAlertJobDefinition :- Job Started -$(date)"
   java -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE -jar /home/ubuntu/scheduler/lenderprice-scheduler-client.jar com.cre8techlabs.scheduler.job.rateAlert.RateAlertJobDefinition >> /home/ubuntu/logs/RateAlertJobDefinitionlogs/`date +\%Y\%m\%d`.log 2>&1
else
    echo "RateAlertJobDefinition :-  ${arr[@]} -Job already processing -$(date)"
fi