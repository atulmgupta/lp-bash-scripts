#!/bin/bash
declare -a arr=( $( ps aux | grep [R]ateAlertForMiniPricerJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -eq 0 ]; then
   echo "RateAlertForMiniPricerJobDefinition :- Job Started -$(date)"
   java -Xmx4096m -Xms256m -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE -jar /home/ubuntu/scheduler/lenderprice-scheduler-client.jar com.cre8techlabs.scheduler.job.rateAlert.miniPricer.RateAlertForMiniPricerJobDefinition >>  /home/ubuntu/logs/RateAlertForMiniPricerJobDefinitionlogs/`date +\%Y\%m\%d`.log 2>&1
else
    echo "RateAlertForMiniPricerJobDefinition :- ${arr[@]} -Job already processing -$(date)"
fi