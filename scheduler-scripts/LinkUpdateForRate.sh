#!/bin/bash
declare -a arr=( $( ps aux | grep [R]ateLinkUpdateJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -eq 0 ]; then
    echo "RateLinkUpdateJobDefinition :- Job Started -$(date)"
   java -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE -jar /home/ubuntu/scheduler/lenderprice-scheduler-client.jar com.cre8techlabs.scheduler.job.rateLinkUpdate.RateLinkUpdateJobDefinition >>  /home/ubuntu/logs/RateLinkUpdateJobDefinitionlogs/`date +\%Y\%m\%d`.log 2>&1
else
	echo "RateLinkUpdateJobDefinition :- ${arr[@]} -Job already processing -$(date)"
fi