#add
#!/bin/bash
declare -a arr=( $( ps aux | grep [A]utoUpdateJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -eq 0]; then
   echo "AutoUpdateJobDefinition :- Job Started -$(date)"
   java -Xmx4096m -Xms256m -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE -jar /home/ubuntu/scheduler/lenderprice-scheduler-client.jar com.cre8techlabs.scheduler.job.rate.update.AutoUpdateJobDefinition >> /home/ubuntu/logs/AutoUpdateJobDefinitionlogs/`date +\%Y\%m\%d`.log 2>&1
else
    echo "AutoUpdateJobDefinition :- ${arr[@]} -Job already processing -$(date)"
fi
