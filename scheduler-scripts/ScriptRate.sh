#!/bin/bash
declare -a arr=( $( ps aux | grep [R]ateLinkUpdateJobDefinition | awk '{print $2}'))

if [ ${#arr[@]} -eq 0 ]; then
   echo  "-job Started- $(date)"
   java  -Xmx4096m -Xms256m -Dspring.profiles.active=stage -jar /home/ubuntu/scheduler/lenderprice-scheduler-client.jar com.cre8techlabs.scheduler.job.rateLinkUpdate.RateLinkUpdateJobDefinition >>  /home/ubuntu/logs/RateLinkUpdateJobDefinition-`date +\%Y\%m\%d`.log 2>&1
else
	echo  "${arr[@]} -Job already processing -$(date)"
fi
