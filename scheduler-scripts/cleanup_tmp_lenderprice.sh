#!/bin/bash
# dkcira, Lender Price 2018
# clean up /tmp directory of temporary files that fill it up. remove only those that fit the patterns
#
# server preparation:
# sudo touch /var/log/lp_cleanup.log
# sudo chown ubuntu:ubuntu /var/log/lp_cleanup.log
# add this line to /etc/crontab
#50 23   * * 1-5 ubuntu  /home/ubuntu/cleanup_tmp_lenderprice.sh 2>&1 >> /var/log/lp_cleanup.log

NRDAYS=7


echo "########################### $(date) $0 cleaning up files $NRDAYS days old."
echo "size of /tmp: $(du -hs /tmp 2>/dev/null)"
echo "df -h /"; df -h /
date

# safer to remove files first
find /tmp -type f -mtime +${NRDAYS} \( -name '_AUTO*' -o -name 'CRE8TECH*' -o -iname 'TEMP*' \) -exec  /bin/rm --interactive=never {} \; 2>/dev/null
# and directories afterwards
find /tmp -maxdepth 1 -type d -mtime +${NRDAYS} \( -name '_AUTO*' -o -name 'CRE8TECH*' -o -iname 'TEMP*' -o -iname '*.xls*' \) -exec  /bin/rmdir {} \; 2>/dev/null

date
echo "df -h /"; df -h /
echo "size of /tmp: $(du -hs /tmp 2>/dev/null)"
echo "$0 End $(date)"
