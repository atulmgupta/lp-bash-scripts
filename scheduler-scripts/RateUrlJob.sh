#!/bin/bash
declare -a arr=( $( ps aux | grep [A]utoUpdateFromUrlJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -eq 0 ]; then
    echo "AutoUpdateFromUrlJobDefinition :- Job Started -$(date)"
   java -Xmx4096m -Xms256m -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE -jar /home/ubuntu/scheduler/lenderprice-scheduler-client.jar com.cre8techlabs.scheduler.job.rate.update.fromUrl.AutoUpdateFromUrlJobDefinition  >>  /home/ubuntu/logs/AutoUpdateJobDefinitionFromUrllogs/`date +\%Y\%m\%d`.log 2>&1
else
	echo "AutoUpdateFromUrlJobDefinition :- ${arr[@]} -Job already processing -$(date)"
fi