#!/bin/bash
declare -a arr=( $( ps aux | grep [A]utoUpdateFromFtpJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -lt 1 ]; then
    echo "AutoUpdateFromFtpJobDefinition :- Job Started -$(date)"
   java -Xmx4096m -Xms256m -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE -jar /home/ubuntu/scheduler/lenderprice-scheduler-client.jar com.cre8techlabs.scheduler.job.rate.update.fromFtp.AutoUpdateFromFtpJobDefinition >>  /home/ubuntu/logs/AutoUpdateFromFtpJobDefinitionlogs/`date +\%Y\%m\%d`.log 2>&1
else
	echo "AutoUpdateFromFtpJobDefinition :- ${arr[@]} -Job already processing -$(date)"
fi
