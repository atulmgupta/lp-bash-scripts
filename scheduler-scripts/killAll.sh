#!/bin/bash
declare -a arr=( $( ps aux | grep [A]utoUpdateFromFtpJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -gt 0 ]; then
     for var in "${arr[@]}"
        do
            echo "${var}"
            kill -9 "${var}"
        done
fi

declare -a arr=( $( ps aux | grep [A]utoUpdateJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -gt 0 ]; then
     for var in "${arr[@]}"
        do
            echo "${var}"
            kill -9 "${var}"
        done
fi

declare -a arr=( $( ps aux | grep [R]ateAlertJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -gt 0 ]; then
     for var in "${arr[@]}"
        do
            echo "${var}"
            kill -9 "${var}"
        done
fi

declare -a arr=( $( ps aux | grep [A]utoUpdateFromFtpJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -gt 0 ]; then
     for var in "${arr[@]}"
        do
            echo "${var}"
            kill -9 "${var}"
        done
fi

declare -a arr=( $( ps aux | grep [R]ateLinkUpdateJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -gt 0 ]; then
     for var in "${arr[@]}"
        do
            echo "${var}"
            kill -9 "${var}"
        done
fi

declare -a arr=( $( ps aux | grep [A]utoUpdateFromUrlJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -gt 0 ]; then
     for var in "${arr[@]}"
        do
            echo "${var}"
            kill -9 "${var}"
        done
fi

declare -a arr=( $( ps aux | grep [R]ateAlertForMiniPricerJobDefinition | awk '{print $2}'))
if [ ${#arr[@]} -gt 0 ]; then
     for var in "${arr[@]}"
        do
            echo "${var}"
            kill -9 "${var}"
        done
fi
